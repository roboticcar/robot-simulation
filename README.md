# Robot Simulation

## Description

- This is a Robot Simulation using webots simulator and ROS2 foxy.
- The Robot is Pioneer 3-AT.
- The Robot uses A LIDAR to do the mapping.
- The Robot uses Cartographer ROS Pacakge to draw occupency grid map.
- I am using a Kinect to see the Robot motion from a 1st person view.
- I am using RVIZ to visualize the map and the robot motion.
- I am using robot localization package to estimate the robot odometry from imu sensor + My odometry estimation node.

## Demo Video

[![Demo Video](https://img.youtube.com/vi/U_lDX4oRf58/0.jpg)](https://www.youtube.com/watch?v=U_lDX4oRf58)

## Dependancies

- Ubuntu 20.04 LTS.
- [ros2 foxy](https://docs.ros.org/en/foxy/Installation/Ubuntu-Install-Debians.html).
- [Webots simulator](https://cyberbotics.com/doc/guide/installation-procedure).
- [Teleop Twist keyboard](https://index.ros.org/p/teleop_twist_keyboard/github-ros2-teleop_twist_keyboard/).
  - To Install it
 ```
 sudo apt update
 sudo apt install ros-foxy-teleop-twist-keyboard
 ```

## Building the project

- Source your ROS setup script.
```
source /opt/ros/foxy/setup.bash
```
- Make a new ros workspace
```
mkdir -p ~/ros_ws/src
```
- Clone the repository
```
cd ~/ros_ws/src
git clone https://gitlab.com/roboticcar/robot-simulation.git
```
- Install the package dependancies
```
cd ~/ros_ws
rosdep install -i --from-path src --rosdistro foxy -y
```
- Build the package
```
colcon build --packages-select robot_simulation  --symlink-install
```
- Source the workspace
```
source ~/ros_ws/install/setup.bash
```

## Running the project

- Run twist keyboard node in the first terminal (To control the robot motion).
```
source /opt/ros/foxy/setup.bash  # Source the ROS installation.
ros2 run teleop_twist_keyboard teleop_twist_keyboard  # Run the keyboard node.
```
- In a second terminal: Run the project Launch file.
```
source /opt/ros/foxy/setup.bash  # Source the ROS installation.
source ~/ros_ws/install/setup.bash  # Source the workspace
ros2 launch robot_simulation robot_launch.py
```

## License

The Project is licenced under MIT License.

## Project Status

The project is Done.
