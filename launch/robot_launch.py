import os
import pathlib
import launch
from launch_ros.actions import Node
from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from webots_ros2_driver.webots_launcher import WebotsLauncher


package_name = 'robot_simulation'


def generate_launch_description() -> LaunchDescription:
    """Packages nodes and exports them to the ROS System for launch

    Returns:
        LaunchDescription: The nodes to be launched.
    """
    # The package share directory
    package_dir = get_package_share_directory(package_name)
    # The robot urdf path
    urdf_file_path = os.path.join(package_dir, 'resource', 'my_robot.urdf')
    # The robot urdf description
    robot_description = pathlib.Path(urdf_file_path).read_text()
    use_sim_time = True

    # The webost simulator launcher
    webots = WebotsLauncher(
        world=os.path.join(package_dir, 'worlds', 'my_world.wbt')
    )
    # The robot driver node, that will drive the robot in webots
    my_robot_driver = Node(
        package='webots_ros2_driver',
        executable='driver',
        output='screen',
        parameters=[
            {'robot_description': robot_description,
             'use_sim_time': use_sim_time,
             'set_robot_state_publisher': True},
        ],
    )
    # The robot state publisher that will publish the robot joints states
    robot_state_publisher = Node(
        package='robot_state_publisher',
        executable='robot_state_publisher',
        output='screen',
        parameters=[{
            'robot_description': '<robot name=""><link name=""/></robot>'
        }],
    )
    # The Extended kalman filter node that will be used to estimate the robot
    # odometry
    ekf_estimator = Node(
        package='robot_localization',
        executable='ekf_node',
        output='screen',
        parameters=[{
            'frequency': 10.0,
            'two_d_mode': True,
            'map_frame': 'map',
            'odom_frame': 'odom',
            'base_link_frame': 'base_link',
            'world_frame': 'odom',
            # Use the imu node and only read the yaw, and the yaw velocity
            'imu0': '/imu',
            'imu0_config': [False, False, False,
                            False, False, True,
                            False, False, False,
                            False, False, True,
                            False, False, False],
            'imu0_queue_size': 40,
            # Use the estimated velocity node
            # listen for x, y, yaw positions, velocity in x and yaw directions
            'odom0': '/odom0',
            'odom0_config': [True, True, False,
                             False, False, True,
                             True, False, False,
                             False, False, True,
                             False, False, False],
            'odom0_queue_size': 40,
            'imu0_differential': False,
            'publish_tf': True,
            # we will not listen to the velocity message so the control_config
            # is useless
            'use_control': False,
            'control_config': [True, False, False,
                               False, False, True],
            'debug': False,
        }],
        remappings=[('/odometry/filtered', '/odom')]
    )
    # The odometry estimator node
    odom_estimator = Node(
        package=package_name,
        executable='odom_estimator',
        output='screen',
        parameters=[{
            'use_sim_time': use_sim_time,
        }],
    )
    cartographer_config_dir = os.path.join(package_dir, 'config')
    configuration_basename = 'my_robot_lds_2d.lua'
    resolution = '0.05'
    publish_period_sec = '1.0'
    rviz_config_file = os.path.join(package_dir, 'config', 'rviz_config.rviz')

    # The cartographer node that will do the 2d-slam mapping
    cartographer_node = Node(
            package='cartographer_ros',
            executable='cartographer_node',
            name='cartographer_node',
            output='screen',
            parameters=[{'use_sim_time': use_sim_time}],
            arguments=['-configuration_directory', cartographer_config_dir,
                       '-configuration_basename', configuration_basename])
    # Another supporting node for the cartographer
    cartographer_occupency_grid_node = Node(
            package='cartographer_ros',
            executable='occupancy_grid_node',
            name='occupancy_grid_node',
            output='screen',
            parameters=[{'use_sim_time': use_sim_time}],
            arguments=['-resolution', resolution,
                       '-publish_period_sec', publish_period_sec])
    # The RViz node for visualization
    rviz_node = Node(
            package='rviz2',
            executable='rviz2',
            name='rviz2',
            arguments=['-d', rviz_config_file],
            parameters=[{'use_sim_time': use_sim_time}],
            output='screen')

    # Launch the nodes and exit all nodes when the simulator exits
    return LaunchDescription([
        webots,
        my_robot_driver,
        odom_estimator,
        robot_state_publisher,
        ekf_estimator,
        cartographer_node,
        cartographer_occupency_grid_node,
        rviz_node,
        launch.actions.RegisterEventHandler(
            event_handler=launch.event_handlers.OnProcessExit(
                target_action=webots,
                on_exit=[
                    launch.actions.EmitEvent(event=launch.events.Shutdown())],
            )
        )
    ])
