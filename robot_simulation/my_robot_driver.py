from math import sqrt
import rclpy
from geometry_msgs.msg import Twist

# Global variables.
# See https://cyberbotics.com/doc/guide/pioneer-3at#pioneer-3-at-model
# for how to set these constants.

# Half the Robot width
HALF_WIDTH = (497/1000)/2
# Half the Robot Length, this is a square Robot
HALF_LENGTH = HALF_WIDTH
# The Rotation radius of the Robot
ROBOT_ROTATION_RADIUS = sqrt(HALF_LENGTH ** 2 + HALF_WIDTH ** 2)
# The Wheel Radius of the Robot
WHEEL_RADIUS = 0.11
# Max Velocity of the Robot in m/s
MAX_VELOCITY = 6.4
# Max Tourque of the Robot in Nm^2
MAX_TORQUE = 20
# Robot Weight in Kg
WEIGHT = 8.8


class MyRobotDriver:
        def init(self, webots_node, properties):
            """This is the node that will be launched by the webot-ros
            bridge.

            Args:
                webots_node: The webot node.
            """
            # Reading the Robot instance from the node
            self.__robot = webots_node.robot

            # Getting the motors from the robot instance
            self.__front_left_motor = self.__robot.getDevice(
                'front left wheel')
            self.__back_left_motor = self.__robot.getDevice('back left wheel')
            self.__front_right_motor = self.__robot.getDevice(
                'front right wheel')
            self.__back_right_motor = self.__robot.getDevice(
                'back right wheel')

            # Intializing the motor positions and velocities
            self.__front_left_motor.setPosition(float('inf'))
            self.__front_left_motor.setVelocity(0)

            self.__back_left_motor.setPosition(float('inf'))
            self.__back_left_motor.setVelocity(0)

            self.__front_right_motor.setPosition(float('inf'))
            self.__front_right_motor.setVelocity(0)

            self.__back_right_motor.setPosition(float('inf'))
            self.__back_right_motor.setVelocity(0)

            # Initializing the robot last recieved velocity message.
            self.__target_twist = Twist()

            # Intialzing the ROS System
            rclpy.init(args=None)
            # Creating the node
            self.__node = rclpy.create_node('my_robot_driver')
            # Subscribing to the velocity message
            self.__node.create_subscription(
                Twist, 'cmd_vel', self.__cmd_vel_callback, 1)

        def __cmd_vel_callback(self, twist: Twist) -> None:
            """The velocity topic callback

            Args:
                twist (Twist): The recieved velocity message.
            """
            self.__target_twist = twist

        def step(self) -> None:
            """The Step function of the node, it is like running the node for
            a timestep.
            """
            # Spin the ros node once.
            rclpy.spin_once(self.__node, timeout_sec=0)

            # Reading the linear amd angular speeds of the last twist message
            forward_speed = self.__target_twist.linear.x
            angular_speed = self.__target_twist.angular.z

            # Computing each motor speed.
            # Here we are computing the velociy as if the robot has only
            # two wheels, and we set the other wheels with the same velocities
            command_motor_left = \
                (forward_speed - angular_speed * ROBOT_ROTATION_RADIUS) / \
                WHEEL_RADIUS
            command_motor_left = min(command_motor_left, MAX_VELOCITY)
            command_motor_right = \
                (forward_speed + angular_speed * ROBOT_ROTATION_RADIUS) / \
                WHEEL_RADIUS
            command_motor_right = min(command_motor_right, MAX_VELOCITY)

            # Setting each motor speed
            self.__front_left_motor.setVelocity(command_motor_left)
            self.__back_left_motor.setVelocity(command_motor_left)
            self.__front_right_motor.setVelocity(command_motor_right)
            self.__back_right_motor.setVelocity(command_motor_right)
